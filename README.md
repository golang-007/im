## 重点

### 依赖包

#### websocket

- golang.org/golang/net/websocket
- github.com/gorilla/websocket (依赖 `golang.org/golang/net/websocket`)

### 鉴权

- userid
- token

userid 与 token 必须匹配, 否则鉴权失败, 不允许操作, status:403

### 前后端数据格式约定

```json
{
  "code": 0, // 0 表示成功, -1: 表示失败
  "message": "错误信息", // 如果code 不为 0 , 则表示失败, 并且 message 有数据,
  "data": {
    // 具体的数据
  }
}
```

## 启动后台

```shell
go build main.go // 可以这样启动
```

或者

```shell
go build // 先编译

nohup main & // 启动
```

## 前端编译

### 安装 `node`

```shell
npm -i // 安装依赖包
```

```shell
npm run build // 编译成静态文件
```

浏览器访问 build 文件夹下面的 index.html 文件即可

## 功能预览

### 登录后首页

<img src="./document/screenshot/home.png" height="500px" width="auto" alt="首页">

### 好友列表

<img src="./document/screenshot/friends.png" height="500px" width="auto" alt="好友列表">

### 好友列表

<img src="./document/screenshot/chat1.png" height="500px" width="auto" alt="聊天界面 1">
<img src="./document/screenshot/chat2.png" height="500px" width="auto" alt="聊天界面 2">
<img src="./document/screenshot/chat3.png" height="500px" width="auto" alt="聊天界面 3">
<img src="./document/screenshot/chat4.png" height="500px" width="auto" alt="聊天界面 4">

### 另外一个 go 开发的 BLOG 系统, 感兴趣的可以看看
[https://gitee.com/golang-007/golang-blog](https://gitee.com/golang-007/golang-blog)
