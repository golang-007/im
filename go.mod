module im

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/websocket v1.4.2
	gopkg.in/fatih/set.v0 v0.2.1
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.8
)
